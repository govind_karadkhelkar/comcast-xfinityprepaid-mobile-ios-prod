function checkLogin()
{
	var b = document.getElementsByClassName('header-my-account__text');
	if (b.length)
	{
        return !(b[0].innerHTML.trim().length == 0 || b[0].innerHTML.search('My Account') != -1 || b[0].innerHTML.search('Mi cuenta') != -1);
	}
	else
	{
		return false;
	}
}

function getName()
{
	var b = document.getElementsByClassName('header-my-account__text');
	if (b.length)
		return b[0].innerHTML.trim();
	else
		return '';
}

function checkCurrentPage()
{
	var b = document.getElementsByClassName('page__title');
	if (b.length)
	{
		if (b[0].innerHTML.search('Welcome back') != -1)
			return 0;
		else if (b[0].innerHTML.search('Refill Prepaid Internet') != -1)
			return 1;
		else
			return -1;
	}
	else
	{
		b = document.getElementsByClassName('page__header');
		if (b.length)
		{
			if (b[0].innerHTML.search('XFINITY Prepaid Support') != -1)
				return 2;
            else
                return -1;
		}
		else
		{
			return -1;
		}
	}
}

function showDashboard()
{
	var b = document.getElementsByClassName('header-my-account__text');
	if (b.length)
		b[0].click();
}

function showRefills()
{
	var b = document.getElementsByTagName('a');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].href.endsWith('CustomerActivation/refill/internet'))
		{
			b[i].click();
            break;
		}
	}
}

function showSupport()
{
	var b = document.getElementsByTagName('a');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].href.endsWith('CustomerActivation/account-secured/support'))
		{
			b[i].click();
            break;
		}
	}
}

function logout()
{
	var b = document.getElementsByClassName('offcanvas-menu__link');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].title == 'Logout')
		{
			b[i].click();
            break;
		}
	}
}

function enrollInAutoRefill()
{
	var b = document.getElementsByClassName('header-my-account__text');
	if (b.length)
		b[0].click();
    
	b = document.getElementsByClassName('link-callout');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].innerHTML.search('ENROLL in AUTO REFILL') != -1)
		{
			b[i].click();
            break;
		}
	}
}

function showAccount()
{
	var b = document.getElementsByTagName('a');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].href.endsWith('account-secured/account-settings'))
		{
			b[i].click();
            break;
		}
	}
}

function managePayments()
{
	var b = document.getElementsByTagName('a');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].href.endsWith('manage-autofill/lookup-autofill'))
		{
			b[i].click();
			break;
		}
	}
}

function showPaymentHistory()
{
	var b = document.getElementsByTagName('a');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].href.endsWith('account-secured/payment-history'))
		{
			b[i].click();
			break;
		}
	}
}

function selectEnglish()
{
	var b = document.getElementsByClassName('offcanvas__menu-footer-link');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].innerHTML.search('English') != -1)
		{
			b[i].click();
			break;
		}
	}
}

function selectSpanish()
{
	var b = document.getElementsByClassName('offcanvas__menu-footer-link');
	for (var i = 0; i < b.length; i++)
	{
		if (b[i].innerHTML.search('Spanish') != -1)
		{
			b[i].click();
			break;
		}
	}
}

function getLanguage()
{
    var b = document.getElementsByTagName('a');
    for (var i = 0; i < b.length; i++)
    {
        if (b[i].href.endsWith('CustomerActivation/account-secured/support'))
        {
            var title = b[i].innerHTML.trim();
            
            if (title == 'Apoyo')
                return "es-MX";
            else
                return "en-US";
        }
    }
}

function hookLoginButton()
{
    var e = document.getElementsByClassName('form-submit-button');
    if (e.length > 0)
    {
        e[0].addEventListener('click', function() {
                              var p = document.getElementById('phone');
                              if (p)
                                window.webkit.messageHandlers.observe.postMessage(p.value);

        });
        
        return 1;
    }
    else
    {
        return 0;
    }
}

(function(doc) {
 
	var addEvent = 'addEventListener',
    type = 'gesturestart',
    qsa = 'querySelectorAll',
    scales = [1, 1],
    meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];
 
	function fix() {
        meta.content = 'width=device-width,user-scalable=no,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
//        doc.removeEventListener(type, fix, true);
	}
 
	if ((meta = meta[meta.length - 1]) && addEvent in doc) {
        fix();
//        scales = [.25, 1.6];
//        doc[addEvent](type, fix, true);
	}
}(document));

