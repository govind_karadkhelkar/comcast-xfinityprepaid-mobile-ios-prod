//
//  AppDelegate.m
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 12/7/17.
//
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "MTBlockAlertView.h"
#import "ViewController.h"
#import "Firebase.h"
#import "PushNotifications.h"

@interface AppDelegate ()
{
@private
    Reachability* _reachability;
    BOOL _connectionFailedShowing;
    BOOL _wasDisconnected;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
    
    [FIRApp configure];
    
    // Register for Push notifications
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert |
     UIUserNotificationTypeBadge |
     UIUserNotificationTypeSound
                                      categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];

    [FIRMessaging messaging].delegate = self;
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReachabilityChangedNotification
                                                  object:nil];
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    _reachability = [Reachability reachabilityWithHostname: @"www.xfinityprepaid.net"];
    [_reachability startNotifier];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    NSLog(@"Received notification: %@", userInfo);
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    
//    NSUInteger l = [deviceToken length];
//    NSMutableString *string = [NSMutableString stringWithCapacity:l * 2];
//    const unsigned char *dataBytes = [deviceToken bytes];
//
//    for (NSInteger i = 0; i < l; i++)
//    {
//        [string appendFormat:@"%02x", dataBytes[i]];
//    }
//
//    self.deviceToken = string;
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)reachabilityChanged:(NSNotification*)note
{
    NetworkStatus status = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    UINavigationController* rootController = (UINavigationController*)self.window.rootViewController;
    ViewController* mainController;
    
    if (rootController)
        mainController = [rootController.viewControllers objectAtIndex:0];
    
    if (status == NotReachable)
    {
        if (!_wasDisconnected)
        {
            _wasDisconnected = YES;
          
            if (mainController)
                [mainController didLooseInternetConnection];

/*
            if (!_connectionFailedShowing)
            {
                _connectionFailedShowing = YES;

                [MTBlockAlertView showWithTitle:@"Warning"
                                        message:@"Internet access currently unavailable. Use of Xfinity Prepaid requires internet access."
                              cancelButtonTitle:@"OK"
                               otherButtonTitle:nil
                                 alertViewStyle:UIAlertViewStyleDefault
                                completionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                    _connectionFailedShowing = NO;
                                }];
            }
 */
        }
    }
    else if (_wasDisconnected)
    {
        _wasDisconnected = NO;

        if (mainController)
            [mainController didAcquireInternetConnection];
    }
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken
{
    self.fcmToken = fcmToken;
//    [PushNotifications registerDeviceForPushNotificationsWithToken:fcmToken withSuccess:^() {} andFailure:^() {} shouldReturnOp:NO];
}

@end
