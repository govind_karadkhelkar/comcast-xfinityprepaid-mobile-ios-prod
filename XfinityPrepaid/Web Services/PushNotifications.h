//
//  PushNotifications.h
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 5/24/18.
//

#import <Foundation/Foundation.h>

@interface PushNotifications : NSObject

+ (id)registerDeviceForPushNotificationsWithToken:(NSString*)token
                                        withPhone:(NSString*)phone
                                   withCultureIso:(NSString*)cultureIso
                                      withSuccess:(void (^)())successBlock
                                       andFailure:(void (^)())failureBlock
                                   shouldReturnOp:(BOOL)returnOp;

@end
