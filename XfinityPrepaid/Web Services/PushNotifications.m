//
//  PushNotifications.m
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 5/24/18.
//

#import "PushNotifications.h"

@implementation PushNotifications

+ (id)registerDeviceForPushNotificationsWithToken:(NSString*)token
                                        withPhone:(NSString*)phone
                                   withCultureIso:(NSString*)cultureIso
                                      withSuccess:(void (^)())successBlock
                                       andFailure:(void (^)())failureBlock
                                   shouldReturnOp:(BOOL)returnOp
{
    NSDictionary *parameters = @{
                                 @"deviceToken": [NSString stringWithFormat:@"\"%@\"", token],
                                 @"userId": [NSString stringWithFormat:@"\"%@\"", phone],
                                 @"action": @"\"add\"",
                                 @"platform": @"\"ios\"",
                                 @"cultureIso": cultureIso
                                 };
    
    NSMutableURLRequest *request = [[WSClient sharedClient] requestWithMethod:@"POST"
                                                                         path:@"Registration"
                                                                   parameters:parameters];
    [request setTimeoutInterval:DEFAULT_TIMEOUT];
    
    // Operation Block Success Method
    void (^opSuccess)(NSURLRequest*, NSHTTPURLResponse*, id) = ^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        if (successBlock) successBlock();
    };
    
    // Operation Block Failure Method
    void (^opFailure)(NSURLRequest*, NSHTTPURLResponse*, NSError*, id) = ^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        if (failureBlock) failureBlock();
    };
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                        success:opSuccess
                                                                                        failure:opFailure];
    
    if(returnOp)
    {
        return  operation;
    }
    else
    {
        [[WSClient sharedClient] enqueueHTTPRequestOperation:operation];
        return nil;
    }
}

@end
