//
//  WSClient.m

#import "WSClient.h"

@implementation WSClient

+ (WSClient *)sharedClient
{
    static WSClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[WSClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        [_sharedClient setAuthorizationHeaderWithUsername:SERVICE_USERNAME password:SERVICE_PASSWORD];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self setParameterEncoding:AFJSONParameterEncoding];
//    [self setDefaultHeader:@"Accept" value:@"application/json"];
    [self setDefaultHeader:@"Content-Type" value:@"application/json"];
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    [self setAllowsInvalidSSLCertificate:YES];
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    return self;
}

@end
