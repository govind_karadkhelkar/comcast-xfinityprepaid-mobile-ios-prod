//
//  WSClient.h
//  HemaGo
//
//  Created by Ryan Suleski on 9/26/13.
//  Copyright (c) 2013 CDMP. All rights reserved.
//

#import "AFHTTPClient.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFHTTPRequestOperation.h"
#import "AFJSONRequestOperation.h"

@interface WSClient : AFHTTPClient

+ (WSClient *)sharedClient;
- (id)initWithBaseURL:(NSURL *)url;

@end
