//
//  TutorialViewController.h
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 5/16/18.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pagerView;
@property (weak, nonatomic) IBOutlet UIButton *getStartedButton;

- (IBAction)getStarted:(id)sender;
@end
