//
//  ViewController.h
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 12/7/17.
//
//

#import <UIKit/UIKit.h>
#import "WebKit/WebKit.h"

@interface ViewController : UIViewController <UITabBarDelegate, WKUIDelegate, WKScriptMessageHandler, UITableViewDelegate, UITableViewDataSource, WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (weak, nonatomic) IBOutlet UITableView *moreView;
@property (weak, nonatomic) IBOutlet UILabel *noInternetLabel;

- (void)didLooseInternetConnection;
- (void)didAcquireInternetConnection;

@end

