//
//  FindRetailerViewController.h
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 12/8/17.
//
//

#import <UIKit/UIKit.h>

@interface FindRetailerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) NSString* url;

@end
