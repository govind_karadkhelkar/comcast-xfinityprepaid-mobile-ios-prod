//
//  TutorialViewController.m
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 5/16/18.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()
{
@private
    BOOL _loadedContent;
}
@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.getStartedButton.layer.borderWidth = 1;
//    self.getStartedButton.layer.borderColor = [UIColor colorWithRed:0 green:0x61 blue:0x9a alpha:1].CGColor;
    self.getStartedButton.layer.cornerRadius = 20;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if (!_loadedContent)
    {
        NSArray* tutorialPages = @[@"t1-%d", @"t2-%d", @"t3-%d"];
        CGSize contentSize;
        
        contentSize.height = self.scrollView.frame.size.height;
        contentSize.width = 0;
        
        for (NSString* imageName in tutorialPages)
        {
            UIImageView* imageView;
            NSInteger imageHeight = 0;
            
            if (contentSize.height == 460 || contentSize.height == 548 ||
                contentSize.height == 647 || contentSize.height == 716 ||
                contentSize.height == 734)
            {
                imageHeight = (NSInteger)contentSize.height;
            }
            
            imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:imageName, imageHeight]]];

            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            imageView.frame = CGRectMake(contentSize.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
            contentSize.width += self.scrollView.frame.size.width;
            [self.scrollView addSubview:imageView];
        }
        
        self.scrollView.contentSize = contentSize;
        self.scrollView.contentOffset = CGPointMake(0, 0);
        self.pagerView.numberOfPages = tutorialPages.count;
        
        _loadedContent = YES;
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    self.pagerView.currentPage = page;
}

- (IBAction)getStarted:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorialShown"];

    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}
@end
