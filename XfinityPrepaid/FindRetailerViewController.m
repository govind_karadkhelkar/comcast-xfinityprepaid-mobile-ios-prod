//
//  FindRetailerViewController.m
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 12/8/17.
//
//

#import "FindRetailerViewController.h"

@interface FindRetailerViewController ()

@end

@implementation FindRetailerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.xfinityprepaid.com/find-a-retailer"]]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
