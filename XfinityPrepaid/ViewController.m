//
//  ViewController.m
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 12/7/17.
//
//

#import "ViewController.h"
#import "MenuViewController.h"
#import "FindRetailerViewController.h"
#import "TutorialViewController.h"
#include "jb.h"
#import "PushNotifications.h"
#import "AppDelegate.h"

static NSString* ROOT_URL = @"https://www.xfinityprepaid.net/CustomerActivation/CustomerActivation?mobile=true";

@interface ViewController ()
{
@private
    WKWebView* _webView;
    BOOL _loggedIn;
    NSString* _name;
    NSInteger _loginHooked;
    NSString* _phone;
    NSString* _currentLanguage;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Xfinity Prepaid";
    
    _currentLanguage = @"en-US";
    
    WKPreferences* prefs = [[WKPreferences alloc] init];
    prefs.javaScriptEnabled = YES;
    prefs.javaScriptCanOpenWindowsAutomatically = YES;
    
    WKWebViewConfiguration* config = [[WKWebViewConfiguration alloc] init];
    config.preferences = prefs;
    
//    WKUserContentController *contentController = [[WKUserContentController alloc] init];
    [config.userContentController addScriptMessageHandler:self name:@"observe"];
//    config.userContentController = contentController;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"xp"
                                                     ofType:@"js"];
    
    NSString *content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:nil];

    WKUserScript* script = [[WKUserScript alloc] initWithSource:content injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    [config.userContentController addUserScript:script];
    
    CGRect webViewFrame = CGRectMake(0, 0, 10, 10);// CGRectMake(0, 0, self.view.frame.size.width, self.tabBar.frame.origin.y);
    
    _webView = [[WKWebView alloc] initWithFrame:webViewFrame configuration:config];
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
//    [_webView setContentHuggingPriority:250 forAxis:UILayoutConstraintAxisVertical];
//    [_webView setContentHuggingPriority:250 forAxis:UILayoutConstraintAxisHorizontal];
//    [_webView setContentCompressionResistancePriority:750 forAxis:UILayoutConstraintAxisVertical];
//    [_webView setContentCompressionResistancePriority:750 forAxis:UILayoutConstraintAxisHorizontal];
//    self.wkWebView.navigationDelegate = self;
    
    [self.view insertSubview:_webView belowSubview:self.moreView];

//    [[_webView.layoutMarginsGuide.leadingAnchor constraintEqualToAnchor:self.tabBar.leadingAnchor] setActive:YES];
//    [[_webView.layoutMarginsGuide.trailingAnchor constraintEqualToAnchor:self.tabBar.trailingAnchor] setActive:YES];
//    [_webView.topAnchor constraintEqualToAnchor:self.moreView.topAnchor].active = YES;
//    [_webView.layoutMarginsGuide.bottomAnchor constraintEqualToAnchor:self.tabBar.layoutMarginsGuide.topAnchor].active = YES;
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:ROOT_URL]]];
    
    self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkState) userInfo:nil repeats:NO];
    
    self.moreView.hidden = YES;
    [self.moreView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.moreView.backgroundColor = [UIColor blackColor];

    if (isJailbroken())
    {
        [_webView stopLoading];
        _webView.hidden = YES;
        _moreView.hidden = YES;
        _noInternetLabel.text = @"This app is only supported on unmodified versions of iOS.";
        
        for (UITabBarItem* item in self.tabBar.items)
            item.enabled = NO;
        
        return;
    }
    
#if !(TARGET_IPHONE_SIMULATOR)
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"tutorialShown"])
        [self showTutorial];
#else
    [self showTutorial];
#endif
}

- (void)showTutorial
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TutorialViewController *vc = [sb instantiateViewControllerWithIdentifier:@"tutorialViewController"];
//    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:NO completion:NULL];
}

- (void)didLooseInternetConnection
{
    [_webView stopLoading];
    _webView.hidden = YES;
    _moreView.hidden = YES;
    
    for (UITabBarItem* item in self.tabBar.items)
        item.enabled = NO;
    
}

- (void)didAcquireInternetConnection
{
    self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
    [self.tabBar.items objectAtIndex:0].enabled = YES;
    [self.tabBar.items objectAtIndex:3].enabled = YES;
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:ROOT_URL]]];
    _webView.hidden = NO;
    _loginHooked = 0;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    CGRect webViewFrame = CGRectMake(0, 0, self.view.frame.size.width, self.tabBar.frame.origin.y);
    
    _webView.frame = webViewFrame;
    [_moreView reloadData];
    
//    if (_moreView)
//    {
//        _moreView.frame = webViewFrame;
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkState
{
    if (!_loginHooked)
    {
        [_webView evaluateJavaScript:@"hookLoginButton();" completionHandler:^(id result, NSError* error)
         {
             if (error == nil)
             {
                 if ([result isKindOfClass:[NSNumber class]])
                 {
                     _loginHooked = ((NSNumber*)result).integerValue;
                 }
             }
         }];
    }

    [_webView evaluateJavaScript:@"getName();" completionHandler:^(id result, NSError* error)
    {
        if (error == nil)
        {
            if ([result isKindOfClass:[NSString class]])
            {
                _name = (NSString*)result;
            }
        }
    }];
    
    [_webView evaluateJavaScript:@"getLanguage();" completionHandler:^(id result, NSError* error)
     {
         if (error == nil)
         {
             if ([result isKindOfClass:[NSString class]])
             {
                 if (![_currentLanguage isEqualToString: (NSString*)result])
                 {
                     _currentLanguage = (NSString*)result;
                     
                     if (_loggedIn)
                     {
                         if (!_phone)
                             _phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                         
                         if (_phone)
                         {
                             AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                             [PushNotifications registerDeviceForPushNotificationsWithToken:appDelegate.fcmToken withPhone:_phone withCultureIso:_currentLanguage withSuccess:^() {} andFailure:^() {} shouldReturnOp:NO];
                         }
                     }
                 }
             }
         }
     }];

    [_webView evaluateJavaScript:@"checkLogin();" completionHandler:^(id result, NSError* error)
    {
        if (error == nil)
        {
            if ([result isKindOfClass:[NSNumber class]])
            {
                BOOL loggedIn = ((NSNumber*)result).boolValue;
                
                if (loggedIn != _loggedIn)
                {
                    _loggedIn = loggedIn;
                    
                    if (_loggedIn)
                    {
                        for (UITabBarItem* item in self.tabBar.items)
                        {
                            if (item.tag == 1 || item.tag == 2)
                                item.enabled = YES;
                        }

                        if (!_phone)
                            _phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];

                        if (_phone)
                        {
                            AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            [PushNotifications registerDeviceForPushNotificationsWithToken:appDelegate.fcmToken withPhone:_phone withCultureIso:_currentLanguage withSuccess:^() {} andFailure:^() {} shouldReturnOp:NO];
                        }
                    }
                    else
                    {
                        for (UITabBarItem* item in self.tabBar.items)
                        {
                            if (item.tag == 1 || item.tag == 2)
                                item.enabled = NO;
                        }
                        
                        if (self.moreView.hidden)
                            self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
                        
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phone"];

                    }
                    
                    if (!self.moreView.hidden)
                        [self.moreView reloadData];
                }
            }
        }
        
//        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkState) userInfo:nil repeats:NO];
    }];

    [_webView evaluateJavaScript:@"checkCurrentPage();" completionHandler:^(id result, NSError* error)
     {
         if (error == nil && self.moreView.hidden)
         {
             if ([result isKindOfClass:[NSNumber class]])
             {
                 NSInteger pageIndex = ((NSNumber*)result).integerValue;
                 
                 if (pageIndex != -1)
                     self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:pageIndex];
             }
         }
         
         [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkState) userInfo:nil repeats:NO];
     }];
}

#pragma mark - WKUIDelegate

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    completionHandler();
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler;
{
    if ([navigationAction.request.URL.absoluteString isEqualToString:@"https://itunes.apple.com/us/app/xfinity-tv/id731629156?ls=1&mt=8"])
    {
        decisionHandler(WKNavigationActionPolicyCancel);
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
    }
    else
    {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

#pragma mark - UITabBarDelegate

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    switch (item.tag)
    {
            
        case 0:
            if (!self.moreView.hidden)
                self.moreView.hidden = YES;
\
            if (_loggedIn)
                [_webView evaluateJavaScript:@"showDashboard();" completionHandler:^(id result, NSError* error) {
                }];
            else
            {
                [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:ROOT_URL]]];
                _loginHooked = 0;
            }
            
            break;
            
        case 1:
            if (!self.moreView.hidden)
                self.moreView.hidden = YES;
            
            [_webView evaluateJavaScript:@"showRefills();" completionHandler:nil];
            
            break;

        case 2:
            if (!self.moreView.hidden)
                self.moreView.hidden = YES;
            
            [_webView evaluateJavaScript:@"showSupport();" completionHandler:nil];
            
            break;
            
        case 3:
        {
            if (self.moreView.hidden)
            {
                self.moreView.hidden = NO;
                [self.moreView reloadData];
            }
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 11;
}

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.preservesSuperviewLayoutMargins = YES;
    cell.contentView.preservesSuperviewLayoutMargins = YES;
    cell.accessoryView = nil;
    
    for (UIView* view in cell.contentView.subviews)
    {
        if (view.tag == 1)
            [view removeFromSuperview];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            if (_loggedIn)
            {
                UILabel* logoutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 20)];
                logoutLabel.text = @"Logout";
                logoutLabel.textColor = [UIColor colorWithRed:44.0 / 255.0 green:142.0 / 255. blue:194.0 / 255.0 alpha:1];
                [logoutLabel sizeToFit];
                logoutLabel.userInteractionEnabled = YES;
                logoutLabel.gestureRecognizers = [NSArray arrayWithObject:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logout:)]];

                
                cell.textLabel.text = _name;
                cell.accessoryView = logoutLabel;
            }
            else
            {
                cell.textLabel.text = @"My Account";
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            break;
        }
            
        case 1:
            cell.textLabel.text = @"Enroll in AutoRefill";
            if (!_loggedIn)
            {
                cell.textLabel.textColor = [UIColor grayColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            break;
            
        case 2:
            cell.textLabel.text = @"My Information";
            if (!_loggedIn)
            {
                cell.textLabel.textColor = [UIColor grayColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            break;
            
        case 3:
            cell.textLabel.text = @"Manage Payments";
            if (!_loggedIn)
            {
                cell.textLabel.textColor = [UIColor grayColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            break;
            
        case 4:
            cell.textLabel.text = @"Payment History";
            if (!_loggedIn)
            {
                cell.textLabel.textColor = [UIColor grayColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            break;
            
        case 5:
        {
            cell.textLabel.text = @"";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            cell.textLabel.frame = CGRectMake(cell.textLabel.frame.origin.x, cell.textLabel.frame.origin.y + 20, cell.textLabel.frame.size.width, cell.textLabel.frame.size.height);
         
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(cell.textLabel.frame.origin.x, cell.textLabel.frame.origin.y + 15, cell.textLabel.frame.size.width, cell.textLabel.frame.size.height)];
            label.tag = 1;
            label.text = @"Additional Links";
            label.textColor = [UIColor whiteColor];
            [cell.contentView addSubview:label];
            
            break;
        }
            
        case 6:
        {
            cell.textLabel.text = @"Find a Retailer";
            
            UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(135, 12, 20, 20)];
            imageView.image = [UIImage imageNamed:@"WebLink"];
            imageView.tag = 1;
            
            [cell.contentView addSubview:imageView];
            
            break;
        }
            
        case 7:
            cell.textLabel.text = @"Activate Service";
            break;

        case 8:
            cell.textLabel.text = @"Privacy";
            break;

        case 9:
            cell.textLabel.text = @"Terms of Service";
            break;

        case 10:
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = @"";
            
            UIButton* engButton = [[UIButton alloc] initWithFrame:CGRectMake(cell.textLabel.frame.origin.x, 12, 59, 20)];
            engButton.titleLabel.textColor = [UIColor whiteColor];
            [engButton setTitle:@"English" forState:UIControlStateNormal];
            [engButton addTarget:self action:@selector(selectEnglish:) forControlEvents:UIControlEventTouchUpInside];
            engButton.tag = 1;
//            [engButton sizeToFit];
            
            [cell.contentView addSubview:engButton];

            UIButton* espButton = [[UIButton alloc] initWithFrame:CGRectMake(95, 12, 64, 20)];
            espButton.titleLabel.textColor = [UIColor whiteColor];
            [espButton setTitle:@"Español" forState:UIControlStateNormal];
            [espButton addTarget:self action:@selector(selectSpanish:) forControlEvents:UIControlEventTouchUpInside];
            espButton.tag = 1;
//            [espButton sizeToFit];
            
            [cell.contentView addSubview:espButton];

            UILabel* separator = [[UILabel alloc] initWithFrame:CGRectMake(82, 10, 10, 20)];
            separator.textColor = [UIColor whiteColor];
            separator.text = @"|";
            separator.tag = 1;
            [cell.contentView addSubview:separator];
            
            break;
        }
            
        default:
            break;
    }
    
    if (indexPath.row < 5)
    {
        cell.backgroundColor = [UIColor colorWithRed:68.0 / 255.0 green:68.0 / 255.0 blue:68.0 / 255.0 alpha:1];
    }
    
    if (indexPath.row > 0 && indexPath.row < 5)
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.row < 6)
    {
        UIView* separator = [[UIView alloc] initWithFrame:CGRectMake(cell.textLabel.frame.origin.x, cell.frame.size.height - 1, cell.frame.size.width - cell.textLabel.frame.origin.x * 2, 1)];
        separator.backgroundColor = [UIColor whiteColor];
        separator.tag = 1;
        
        [cell.contentView addSubview:separator];
    }

    return cell;
}

- (void)logout:(id)sender
{
    if (_loggedIn)
    {
        [_webView evaluateJavaScript:@"logout();" completionHandler:nil];
        
        _loginHooked = 0;
        self.moreView.hidden = YES;
        self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
    }
}

- (void)selectEnglish:(UIButton*)sender
{
    [_webView evaluateJavaScript:@"selectEnglish();" completionHandler:nil];

    self.moreView.hidden = YES;
    self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
}

- (void)selectSpanish:(UIButton*)sender
{
    [_webView evaluateJavaScript:@"selectSpanish();" completionHandler:nil];

    self.moreView.hidden = YES;
    self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.row)
    {
        case 0:
//            if (_loggedIn)
//            {
//                [_webView evaluateJavaScript:@"logout();" completionHandler:nil];
//
//                _loginHooked = 0;
//                self.moreView.hidden = YES;
//                self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
//            }
            
            break;
            
        case 1:
            if (_loggedIn)
            {
                [_webView evaluateJavaScript:@"showDashboard();enrollInAutoRefill();" completionHandler:nil];

                self.moreView.hidden = YES;
                self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
            }
            
            break;
            
        case 2:
            if (_loggedIn)
            {
                [_webView evaluateJavaScript:@"showAccount();" completionHandler:nil];
                
                self.moreView.hidden = YES;
                self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
            }
            
            break;
            
        case 3:
            if (_loggedIn)
            {
                [_webView evaluateJavaScript:@"managePayments();" completionHandler:nil];
                
                self.moreView.hidden = YES;
                self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
            }
            
            break;
            
        case 4:
            if (_loggedIn)
            {
                [_webView evaluateJavaScript:@"showPaymentHistory();" completionHandler:nil];
                
                self.moreView.hidden = YES;
                self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
            }
            
            break;
            
        case 5:
//            if (_loggedIn)
//            {
//                [_webView evaluateJavaScript:@"showAdditionalLinks();" completionHandler:nil];
                
//                self.moreView.hidden = YES;
//                self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
//            }
            
            break;
            
        case 6:
        {
            FindRetailerViewController* controller = [[FindRetailerViewController alloc] initWithNibName:@"FindRetailerViewController" bundle:nil];
            
            controller.url = @"https://www.xfinityprepaid.com/find-a-retailer";
            [self.navigationController pushViewController:controller animated:YES];
            
            break;
        }
            
        case 7:
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:ROOT_URL]]];
            _loginHooked = 0;
            
            self.moreView.hidden = YES;
            self.tabBar.selectedItem = [self.tabBar.items objectAtIndex:0];
            
            break;

        case 8:
        {
            FindRetailerViewController* controller = [[FindRetailerViewController alloc] initWithNibName:@"FindRetailerViewController" bundle:nil];
            
            controller.url = @"https://www.xfinity.com/corporate/legal/privacystatement";
            [self.navigationController pushViewController:controller animated:YES];
            
            break;
        }
            
        case 9:
        {
            FindRetailerViewController* controller = [[FindRetailerViewController alloc] initWithNibName:@"FindRetailerViewController" bundle:nil];
            
            controller.url = @"https://www.xfinity.com/corporate/legal/visitoragreement";
            [self.navigationController pushViewController:controller animated:YES];
            
            break;
        }

        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 5)
        return 74;
    else
        return 44;
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    _phone = message.body;

    [[NSUserDefaults standardUserDefaults] setObject:_phone forKey:@"phone"];
}

//
@end
