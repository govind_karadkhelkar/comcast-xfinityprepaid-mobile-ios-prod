//
//  Globals.h
//  Claims
//
//  Created by Serguei Soltan on 7/15/14.
//  Copyright (c) 2014 Petplan. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_IPHONE4  (([UIScreen mainScreen].bounds.size.height * [UIScreen mainScreen].scale) < 1136.0f)


#ifdef DEBUG
    #define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
    #define FLog(view) DLog(@"%@ frame: %@", view, CGRectCreateDictionaryRepresentation([view frame]))
    #define BLog(view) DLog(@"%@ bounds: %@", view, CGRectCreateDictionaryRepresentation([view bounds]))
#else
    #define DLog(...)
    #define FLog(...)
    #define BLog(...)
#endif


#define NSStringFromBool(b) (b ? @"YES" : @"NO")

#ifndef NS_ENUM
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#endif

#define NIL_IF_NULL(foo) ((foo == [NSNull null]) ? nil : foo)
#define NULL_IF_NIL(foo) ((foo == nil) ? [NSNull null] : foo)
#define EMPTY_IF_NIL(foo) ((foo == nil) ? @"" : foo)
#define NULL_IF_ZERO(foo) (([foo intValue] == 0) ? [NSNull null] : foo)
#define ZERO_IF_NIL(foo) ((foo == nil) ? @"0" : foo)

#define IS_POPULATED_ID_FIELD(id) (id && id != [NSNull null] && id > 0)

#define CONTAINS_KEY(dict, key) [[dict allKeys] containsObject: key]

extern const NSTimeInterval DEFAULT_TIMEOUT;

extern NSString * const BASE_URL;
extern NSString * const BASE_URL_WITH_CREDENTIALS;

extern NSString * const SERVICE_USERNAME;
extern NSString * const SERVICE_PASSWORD;

@interface Globals : NSObject

@end
