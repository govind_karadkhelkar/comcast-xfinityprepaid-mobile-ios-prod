//
//  AppDelegate.h
//  XfinityPrepaid
//
//  Created by Serguei Soltan on 12/7/17.
//
//

#import <UIKit/UIKit.h>
#import "Firebase.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow* window;
@property (strong, nonatomic) NSString* fcmToken;


@end

